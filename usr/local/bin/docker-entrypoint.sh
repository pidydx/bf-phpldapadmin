#!/bin/bash

set -e

if [ "$1" = 'nginx' ]; then
    exec nginx -g "daemon off;"
fi

if [ "$1" = 'php-fpm' ]; then
    exec php-fpm8.3 -F
fi

exec "$@"