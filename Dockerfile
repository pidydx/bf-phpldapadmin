#########################
# Create base container #
#########################
FROM ubuntu:24.04 as base
LABEL maintainer="pidydx"

# Set app user and group
ENV APP_USER=nginx
ENV APP_GROUP=nginx

# Set version pins
ENV VERSION=1.2.6.7

# Set base dependencies
ENV BASE_DEPS ca-certificates nginx php-fpm php-ldap php-xml

# Set build dependencies
ENV BUILD_DEPS wget

# Create app user and group
RUN userdel ubuntu
RUN groupadd -g 1000 ${APP_GROUP} \
 && useradd -r -M -N -u 1000 ${APP_USER} -g ${APP_GROUP} -s /usr/sbin/nologin

# Update and install base dependencies
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get upgrade -yq \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BASE_DEPS} \
 && rm -rf /var/lib/apt/lists/*


##########################
# Create build container #
##########################
FROM base AS builder

RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BUILD_DEPS}

# Run build
COPY build.sh /usr/src/build.sh
WORKDIR /usr/src
RUN ./build.sh


##########################
# Create final container #
##########################
FROM base

# Finalize install
COPY etc/ /etc/
COPY --from=builder /usr/local /usr/local/
COPY usr/ /usr/

RUN update-ca-certificates \
 && mkdir -p /run/nginx \
 &&	chown ${APP_USER}:${APP_GROUP} /run/nginx \
 && mkdir -p /run/php \
 &&	chown ${APP_USER}:${APP_GROUP} /run/php \
 && ln -sf /dev/stdout /var/log/nginx/access.log \
 && ln -sf /dev/stderr /var/log/nginx/error.log \
 && ln -s /etc/phpldapadmin /usr/local/share/phpldapadmin/config


# Prepare container
EXPOSE 8080/tcp
VOLUME ["/etc/phpldapadmin"]
USER $APP_USER

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["nginx"]

