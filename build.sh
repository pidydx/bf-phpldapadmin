#!/bin/bash

set -e

wget -nv https://github.com/leenooks/phpLDAPadmin/archive/${VERSION}.tar.gz
tar -zxf ${VERSION}.tar.gz
mv phpLDAPadmin-${VERSION} /usr/local/share/phpldapadmin
rm -Rf /usr/local/share/phpldapadmin/config
